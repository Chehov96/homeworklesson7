
function exerciseOne() {
   for (let i = 1; i <= 100; i++) {
      if ((i % 5 == 0) && (i % 3 == 0)) {
         console.log("FizzBuzz");
         continue
      }
      if (i % 3 == 0) {
         console.log("Fizz");
         continue
      }
      if (i % 5 == 0) {
         console.log("Buzz");
         continue
      }
      console.log(i);
   }
}

let str = 'Лето удалось очень жарким, было много ягод и фруктов, лучшим местом для отдыха былa речка';
function exerciseTwoDotOne() {
   console.log("Выводим в консоль исходную строку:");
   console.log("'" + str + "'");
   console.log(" ") //для читаемости в консоли
   console.log("Заменим все запятые на точки:");
   let strWithDot = str.replace(/\,/g, ".");
   console.log("'" + strWithDot + "'")
   console.log(" ") //для читаемости в консоли
   console.log("Заменим первые буквы у слов после запятых на заглавные (в соответствии с заданием):")
   let newStr = str.replace(/(\,\s+)(.)/g, (match, b, c) => {
      return b + c.toUpperCase();
   });
   console.log("'" + newStr + "'");
   console.log(" ") //для читаемости в консоли
   console.log("А лучше заменим и запятые на точки, и строчные буквы после точек на заглавные:")
   let newestStr = str.replace(/(\,\s+)(.)/g, (match, b, c) => {
      return b.replace(/\,/, ".") + c.toUpperCase();
   });
   console.log("'" + newestStr + "'");
}

let strBirthday = 'Антон, Дима и Женя пришли на день рождения сразу, а Миша пришел позже';
let separator = /\,| /;
function exerciseTwoDotTwo() {
   console.log("Выводим в консоль исходную строку:");
   console.log("'" + strBirthday + "'");
   console.log(" ") //для читаемости в консоли
   let arrBirthday = strBirthday.split(separator);
   console.log('Разделитель: "' + separator + '"' + ' (то есть запятая или пробел)');
   console.log('Массив содержит ' + arrBirthday.length + ' элементов: ' + arrBirthday.join(' / '));
   console.log(" ") //для читаемости в консоли
   console.log("Выводим в консоль новый массив без лишних элементов:");
   let newArrBirthDay = arrBirthday.filter(value => { return value !== "" && value !== "и" && value !== "а" && value !== "пришел" && value !== "сразу" && value !== "позже" });
   console.log(newArrBirthDay.join(' / '));
   console.log(" ") //для читаемости в консоли
   console.log(newArrBirthDay.join(' / '));
   console.log(" ") //для читаемости в консоли
   console.log("Меняем порядок элементов на новый:");
   [newArrBirthDay[4], newArrBirthDay[0]] = [newArrBirthDay[0], newArrBirthDay[4]];
   [newArrBirthDay[5], newArrBirthDay[1]] = [newArrBirthDay[1], newArrBirthDay[5]];
   [newArrBirthDay[6], newArrBirthDay[2]] = [newArrBirthDay[2], newArrBirthDay[6]];
   newArrBirthDay[0] = 'На';
   console.log(newArrBirthDay.join(' / '));
   console.log(" ") //для читаемости в консоли
   console.log("Преобразуем массив в строку обычным сложением элементов (я знаю, что можно лучше, но ничего лучше не придумал):");
   let newStrBirthday = newArrBirthDay[0] + ' ' + newArrBirthDay[1] + ' ' + newArrBirthDay[2] + ' ' + newArrBirthDay[3] + ': ' + newArrBirthDay[4] + ', ' + newArrBirthDay[5] + ', ' + newArrBirthDay[6] + ', ' + newArrBirthDay[7] + '.';
   console.log(newStrBirthday);
}
function exerciseTwoDotThree() {
   let word = prompt('Введите слово или строку, которое (-ую) хотите проверить на палиндромность', '');
   function isPalindrome(word) {
      var normalized = word.toLowerCase().match(/[a-z]|[а-я]/gi).reverse();
      return normalized.join('') === normalized.reverse().join('');
   }
   if (isPalindrome(word)) {
      alert('Ваше словечко является палиндромом!')
   } else {
      alert('Ваше словечко НЕ является палиндромом!')
   }
}
function clearAfterExercise() {
   console.clear();
}
function doesNotCleanAnything() {
   let user = prompt('Введите ваше имя, пожалуйста', '')
   if (user !== null) {
      alert('Уважаемый, ' + user + '! ' + 'В третьем задании не используется консоль!')
   } else {
      alert('Уважаемый Инкогнито, я все равно обязан сказать, что в третьем задании не используется консоль!')
   }

}